#!/usr/bin/python
import time
start_time = time.time()

from ds18b20 import PiDS18B20

hardware_id = "28-0000055d861a"
mysensor = PiDS18B20(hardware_id)

print 'Sensor ' + mysensor.id + ' shows a reading of ' + str(mysensor.ReadTempF) + 'F'
# elapsed_time = time.time() - start_time
print ('Elapsed time = ' + str(time.time() - start_time) + ' seconds')
