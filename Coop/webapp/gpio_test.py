#!/usr/bin/python
import time
from gpio import PiGpio

# create an instance of the pi gpio driver.
pi_gpio= PiGpio()

# Blink the LEDS forever.
print('Checking the Hall Effect sensor state (Ctrl-C to stop)...')
while True:
# Get the current switch state and print
    switch = pi_gpio.read_HALL()
    print('\n============ Switch: {0} ============'.format(switch))
