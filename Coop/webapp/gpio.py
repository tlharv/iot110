import RPi.GPIO as GPIO

U_HALL_PIN = 17 # upper hall effect sensor (GPIO17)

class PiGpio(object):
    """Raspberry Pi Internet 'IoT GPIO'."""

    def __init__(self):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(U_HALL_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    def read_HALL(self):
        """Read the Hall Effect sensor state."""
        switch = GPIO.input(U_HALL_PIN)
        # invert because of active low momentary switch
        if (switch == 0):
            switch=1
        else:
            switch=0
        return switch
