[LAB5 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab5/setup.md)

## Part C - Display Time, Temp and Pressure in a Bootstrap Table Structure
**Synopsis:** We now need to add a few new JavaScript libraries and stylesheets
in order to spruce up our user interface for displaying data. 

## Objectives
* Download jQuery-UI, RaphaelJS and MorrisJS and install in static assets
* Configure a "Tabbed" Panel for our Data Dashboard
* Shrink and Optimize the LED and Switch Dashboard
* JavaScript array object to handle data feed

### Step C1: Download Tools (JS and CSS) jQuery-UI, RaphaelJS and MorrisJS
Download the above libraries and copy the ```min.css``` and ```min.js``` files 
(if ```minified``` is availabl) into the static assets folder. The folder should 
look like this after downloading and copying assets from the above libaries:

```
pi$ ls -l static/css/
total 176
-rw-r--r-- 1 pi pi 121200 Feb 11 16:38 bootstrap.min.css
-rw-r--r-- 1 pi pi  32076 Sep 14 18:34 jquery-ui.min.css
-rw-r--r-- 1 pi pi  13849 Sep 14 18:34 jquery-ui.theme.min.css
-rw-r--r-- 1 pi pi    986 Feb 12 10:22 lab5.css
-rwxr-xr-x 1 pi pi    433 Jun 15  2014 morris.css

pi$ ls -l static/js/
total 512
-rw-r--r-- 1 pi pi  37045 Feb 11 16:38 bootstrap.min.js
-rw-r--r-- 1 pi pi  86709 Feb 11 16:38 jquery-3.1.1.min.js
-rw-r--r-- 1 pi pi 253669 Sep 14 18:34 jquery-ui.min.js
-rw-r--r-- 1 pi pi   5552 Feb 12 15:34 lab5.js
-rwxr-xr-x 1 pi pi  35652 Jun 15  2014 morris.min.js
-rw-r--r-- 1 pi pi  93251 Feb  9 20:36 raphael.min.js
```

### Step C2: New Dashboard!
As we try to pack more sensor data types and displays into our small display, we
need to find a better UI element to display. Enter the "Tabbed" display! 
Fortunately we don't have to work hard at all to have a tab based panel display.
The above libraries provide us with a simple way to have this multi-tab interface.

![Dashboard with jQuery-UI](https://gitlab.com/iot110/iot110-student/raw/master/Resources/Images/Dashboard-jQuery-UI.png)

Rather than detail out the Web design and coding steps (since this isn't a Web
design class) let's focus on just using this capability and doing just the small
amount of coding required to integrate what we coded in class already for tablular
data display.

### Step C3: GIT PULL and Let's GO!
We want to keep pace with next labs and there's quite a bit of UI tweaking required
to get the display just the way we want it for this stage.  Therefore just do a
git pull and you'll be 100% ready to move on to the homework of integrating the
pressure data chart.

[PART D](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab5/PartD.md) Add MorrisJS Charting Capability for the Pressure

[LAB5 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab5/setup.md)
