#!/usr/bin/python
import pprint
from bmp280 import PiBMP280

# In python, dictionaries let you define arrays where the elements aren't
# numerical positions, but you can name them.

# Step C2 - Array of BMP280 Sensor Parameters
# create an array of my pi bmp280 sensor dictionaries
# key value pairs
sensor = []
sensor.append({'name': 'bmp280', 'addr': 0x76, 'chip':PiBMP280(0x76), 'data': {}})

# Step C3 - Get Sensor ID Populated
# Read the sensor ID for 0x76 -> values into the ['data'] dictionary
(chip_id, chip_version) = sensor[0]['chip'].readBMP280ID()
sensor[0]['data']['chip_id'] = chip_version
sensor[0]['data']['chip_version'] = chip_version

# Step C4 - Get the sensor data and print the object
print "  ============================== SENSOR   =============================="
print "  Chip ADDR :", hex(sensor[0]['addr'])
print "    Chip ID :", sensor[0]['data']['chip_id']
print "    Version :", sensor[0]['data']['chip_version']

# Read the Sensor Temp/Pressure values into the ['data'] dictionary
(temperature, pressure) = sensor[0]['chip'].readBMP280All()
sensor[0]['data']['temperature'] = { 'reading': temperature, 'units' : 'C' }
sensor[0]['data']['pressure'] = { 'reading': pressure, 'units' : 'hPa' }

print "Temperature :", sensor[0]['data']['temperature']['reading'], "C"
print "   Pressure :", sensor[0]['data']['pressure']['reading'] , "hPa"

pprint.pprint(sensor[0])
print "  ======================================================================"
